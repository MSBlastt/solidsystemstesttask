package com.task.services;

import com.task.config.DataConfigForTest;
import com.task.service.ProductService;
import com.task.service.SaleService;
import com.task.service.sample.SampleDataGenerator;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataConfigForTest.class)
@WebAppConfiguration
@Ignore
public class EntityAndServiceBaseTest {

    @Resource
    protected EntityManagerFactory emf;

    protected EntityManager em;

    @Resource
    protected ProductService productService;

    @Resource
    protected SaleService saleService;

    @Resource
    SampleDataGenerator sampleDataGenerator;


    public void generateSampleData()
    {
        sampleDataGenerator.generateSampleData();
    }

}
