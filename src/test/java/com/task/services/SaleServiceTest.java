package com.task.services;

import com.task.entity.Product;
import com.task.entity.Sale;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;


public class SaleServiceTest extends EntityAndServiceBaseTest {



    @Test
    public void checkSaleServiceTest() throws Exception {
        Product product = new Product();
        product.setName("Test_Tovar_For_Sale");
        product.setPrice(500);
        productService.addProduct(product);
        Sale sale = new Sale();
        sale.setDate(new Date());
        sale.setDiscount(5);
        sale.setAmount(2);
        sale.setProducts(new HashSet<Product>(){{
        add(product);
        }
        });
        saleService.addSale(sale);
        Sale sale1 = saleService.getSale(sale.getId());
        assertEquals(sale.getAmount(), sale1.getAmount());
        assertEquals(sale.getId(), sale1.getId());
        assertEquals(sale.getDate(), sale1.getDate());
        assertEquals(sale.getDiscount(), sale1.getDiscount());
        assertEquals(sale.getProducts().iterator().next().getName(), sale1.getProducts().iterator().next().getName());
        assertEquals(sale.getProducts().iterator().next().getPrice(), sale1.getProducts().iterator().next().getPrice());
        assertEquals(sale.getProducts().iterator().next().getId(), sale1.getProducts().iterator().next().getId());
        saleService.removeSale(sale.getId());
        System.out.println("Objects are identical!");
    }


}
