package com.task.services;

import com.task.entity.Product;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ProductServiceTest extends EntityAndServiceBaseTest {



    @Test
    public void checkProductServiceTest() throws Exception {
        Product product = new Product();
        product.setName("Test_Tovar");
        product.setPrice(100);
        productService.addProduct(product);
        Product product1 = productService.getProduct(product.getId());
        assertEquals(product.getName(), product1.getName());
        assertEquals(product.getId(), product1.getId());
        assertEquals(product.getPrice(), product1.getPrice());
        productService.removeProduct(product.getId());
        System.out.println("Objects are identical!");
    }


}
