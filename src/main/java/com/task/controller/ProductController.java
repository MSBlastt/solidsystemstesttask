package com.task.controller;

import com.task.entity.Product;
import com.task.entity.Sale;
import com.task.service.ProductService;
import com.task.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.Set;

@Controller
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    SaleService saleService;

    @RequestMapping(value = "/addproduct", method = RequestMethod.POST)
    public String addProduct(@ModelAttribute(value = "product") Product product)

    {
        productService.addProduct(product);
        Long productsId = product.getId();

        return "redirect:" + "/";
    }

    @RequestMapping(value = "/buyproduct", method = RequestMethod.POST)
    public String buyProduct(@RequestParam(value = "prId") Product product,
                             @RequestParam(value = "prAmount") Integer amount,
                             @RequestParam(value = "prDiscount") Integer discount)
    {
        Sale sale = new Sale();
        sale.setAmount(amount);
        sale.setDiscount(discount);
        Set<Product> products = new HashSet<>();
        products.add(product);
        sale.setProducts(products);
        saleService.addSale(sale);
        return "redirect:" + "/";
    }

    @RequestMapping(value = "/editName", method = RequestMethod.POST)
    public String editProductName(@RequestParam(value = "apId") Product product,
                                  @RequestParam(value = "apName") String name){
        product.setName(name);
        productService.updateProduct(product);
        return "redirect:" + "/";
    }


    @RequestMapping(value = "/editPrice", method = RequestMethod.POST)
    public String editproductsName(@RequestParam(value = "apId") Product product,
                                   @RequestParam(value = "apPrice") Integer price){
        product.setPrice(price);
        productService.updateProduct(product);
        return "redirect:" + "/";
    }

    @RequestMapping(value = "/moderation/products/delete", method = RequestMethod.GET)
    public String deleteProduct(@RequestParam(value = "id") Product product)
    {
        productService.removeProduct(product.getId());
        return "redirect:" + "/";
    }


    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updateProduct(@RequestParam(value="id") Long productId, ModelMap model){

        Product Product;
        try {
            Product = productService.getProduct(productId);
            Product.getId();
        }catch (Exception e){
            return "/";
        }

        return "redirect:" + "/";
    }
}
