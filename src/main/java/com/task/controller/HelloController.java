package com.task.controller;

import com.task.entity.Product;
import com.task.entity.Sale;
import com.task.service.ProductService;
import com.task.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

@Controller
public class HelloController extends TemplateController {

    @Autowired
    ProductService productService;

    @Autowired
    SaleService saleService;

    private Product luckyProduct;

    private Double luckyDiscount;

    private Date luckyProductTimeStamp;

    private Date luckyProductEndTime;

    @RequestMapping("/")
    public String startPage(ModelMap model) {
        addHeaderAttributes(model);

        List<Product> products = productService.getAllProducts();

        model.addAttribute("allProducts", products);
        model.addAttribute("allSales", saleService.getAllSales());
        model.addAttribute("product",new Product());
        model.addAttribute("sale",new Sale());
        model.addAttribute("discountMeter",saleService.getDiscountMeter());
        model.addAttribute("hourStatistics",saleService.getDetailedHourStatisticForLastDay());

        //Lucky product
        if (luckyProduct == null) luckyProduct = products.get((int) ((Math.random() * products.size())));
        if (luckyDiscount == null) luckyDiscount = Double.valueOf(5 + (Math.random() * (10 - 5)));
        if (luckyProductTimeStamp == null) {
            luckyProductTimeStamp = new Date();
            luckyProductEndTime = new Date(luckyProductTimeStamp.getTime() + 3600000l);
        }

        int minutesRemaining = (int) (((luckyProductEndTime.getTime() - new Date().getTime())/1000)/60);

        if (minutesRemaining < 1) {
            luckyProduct = null;
            luckyProductTimeStamp = null;
            luckyDiscount = null;
        }

        model.addAttribute("luckyProduct",luckyProduct);
        model.addAttribute("luckyProductDiscount", luckyDiscount);
        model.addAttribute("minutesRemaining", minutesRemaining);

        return "interface/onStart";

    }



}
