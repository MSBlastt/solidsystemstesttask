package com.task.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price", nullable = false)
    private Integer price;


    @ManyToMany(mappedBy = "products", cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.EAGER)
    private Set<Sale> sales;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (!this.id.equals(product.id)) return false;
        if (!this.name.equals(product.name)) return false;
        if (!this.price.equals(product.price)) return false;
        if (this.sales != null ? !this.sales.equals(product.sales) : product.sales != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = id != null ? this.id.hashCode() :  0;
        result = name != null ? result = 31 * result + this.name.hashCode() : 0;
        result = price !=null ? 31 * result + this.price.hashCode(): 0;
        result = sales != null ? 31 * result + (this.sales != null ? this.sales.hashCode() : 0): 0;
        return result;
    }
}
