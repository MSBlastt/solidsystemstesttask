package com.task.entity.helpers;

import java.util.Date;

public class HourStatistic {
    private Date date;
    private Integer bills;
    private Double billsSum;
    private Double discountSum;

    public Double getDiscountSum() {
        if (this.discountSum == null) discountSum = 0d;
        return this.discountSum;
    }

    public void setDiscountSum(Double discountSum) {
        this.discountSum = discountSum;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getBills() {
        if (this.bills == null) bills = 0;
        return this.bills;
    }

    public void setBills(Integer bills) {
        this.bills = bills;
    }

    public Double getBillsSum() {
        if (this.billsSum == null) billsSum = 0d;
        return this.billsSum;
    }

    public void setBillsSum(Double billsSum) {
        this.billsSum = billsSum;
    }

}