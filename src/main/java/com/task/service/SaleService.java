package com.task.service;

import com.task.entity.Sale;
import com.task.entity.helpers.HourStatistic;

import java.util.List;

public interface SaleService {

    List<Sale> getAllSales();
    Sale addSale (Sale sale);
    Sale getSale (Long id);
    void removeSale (Long id);
    Double getDiscountMeter();
    List<HourStatistic> getDetailedHourStatisticForLastDay();
}

