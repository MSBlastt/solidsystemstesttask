package com.task.service;


import com.task.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();
    Product addProduct(Product Product);
    Product getProduct(Long id);
    Product updateProduct(Product Product);
    void removeProduct(Long id);
    Product getLuckyProduct();

}
