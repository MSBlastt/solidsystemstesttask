package com.task.service.sample;

import com.task.entity.Product;
import com.task.entity.Sale;
import com.task.service.ProductService;
import com.task.service.SaleService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.HashSet;

@Service
public class SampleDataGenerator {
	@Inject
	protected EntityManagerFactory emf;
	protected EntityManager em;

	@Inject
	protected ProductService productService;

	@Inject
	protected SaleService saleService;


	public void generateSampleData() {
		em = emf.createEntityManager();

		// create 7 products

        Product product1 = new Product();
        product1.setName("Tovar_1");
        product1.setPrice(100);
        Product product2 = new Product();
        product2.setName("Tovar_2");
        product2.setPrice(200);
        Product product3 = new Product();
        product3.setName("Tovar_3");
        product3.setPrice(300);
        Product product4 = new Product();
        product4.setName("Tovar_4");
        product4.setPrice(400);
        Product product5 = new Product();
        product5.setName("Tovar_5");
        product5.setPrice(500);
        Product product6 = new Product();
        product6.setName("Tovar_6");
        product6.setPrice(600);
        Product product7 = new Product();
        product7.setName("Tovar_7");
        product7.setPrice(700);
        productService.addProduct(product1);
        productService.addProduct(product2);
        productService.addProduct(product3);
        productService.addProduct(product4);
        productService.addProduct(product5);
        productService.addProduct(product6);
        productService.addProduct(product7);


        //create 7 sales

        Sale sale1 = new Sale();
        sale1.setProducts(new HashSet<Product>(){{
            add(product1);
        }});
        sale1.setAmount(1);
        sale1.setDiscount(0);
        sale1.setDate(new Date((new Date().getTime()-25000000)));

        Sale sale2 = new Sale();
        sale2.setProducts(new HashSet<Product>(){{
            add(product3);
        }});
        sale2.setAmount(2);
        sale2.setDiscount(3);
        sale2.setDate(new Date((new Date().getTime()-38000000)));

        Sale sale3 = new Sale();
        sale3.setProducts(new HashSet<Product>(){{
            add(product4);
        }});
        sale3.setAmount(3);
        sale3.setDiscount(5);
        sale3.setDate(new Date((new Date().getTime()-70000000)));

        Sale sale4 = new Sale();
        sale4.setProducts(new HashSet<Product>(){{
            add(product5);
        }});
        sale4.setAmount(4);
        sale4.setDiscount(7);
        sale4.setDate(new Date((new Date().getTime()-50000000)));

        Sale sale5 = new Sale();
        sale5.setProducts(new HashSet<Product>(){{
            add(product5);
        }});
        sale5.setAmount(5);
        sale5.setDiscount(8);
        sale5.setDate(new Date((new Date().getTime()-86000000)));

        Sale sale6 = new Sale();
        sale6.setProducts(new HashSet<Product>(){{
            add(product6);
        }});
        sale6.setAmount(6);
        sale6.setDiscount(9);
        sale6.setDate(new Date(new Date().getTime()-27000000));

        Sale sale7 = new Sale();
        sale7.setProducts(new HashSet<Product>(){{
            add(product7);
        }});
        sale7.setAmount(7);
        sale7.setDiscount(10);
        sale7.setDate(new Date(new Date().getTime()-84000000));

        saleService.addSale(sale1);
        saleService.addSale(sale2);
        saleService.addSale(sale3);
        saleService.addSale(sale4);
        saleService.addSale(sale5);
        saleService.addSale(sale6);
        saleService.addSale(sale7);

	}

}
