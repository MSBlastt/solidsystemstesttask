package com.task.service.impl;

import com.task.entity.Sale;
import com.task.entity.helpers.HourStatistic;
import com.task.repository.SaleRepository;
import com.task.service.SaleService;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {

    @Autowired
    private SaleRepository saleRepository;

    @Override
    public List<Sale> getAllSales() {
        List<Sale> sales = saleRepository.findAll();
        sales.sort(new Comparator<Sale>() {
            @Override
            public int compare(Sale o1, Sale o2) {
               boolean before = o2.getDate().before(o1.getDate());
                return before ? -1 : 1;
            }
        });
        return sales;
    }

    @Override
    public Sale addSale(Sale sale) {
        return saleRepository.saveAndFlush(sale);
    }

    @Override
    public Sale getSale(Long id) {
        return saleRepository.findOne(id);
    }

    @Override
    public void removeSale(Long id) {
        saleRepository.delete(id);
    }

    @Override
    public Double getDiscountMeter() {
        List<Sale> sales = saleRepository.findAll();
        Double result = 0d;
        for (Sale sale : sales){
            result += ((sale.getAmount()*sale.getProducts().iterator().next().getPrice())/100)*sale.getDiscount();
        }
        return result;
    }

    @Override
    public List<HourStatistic> getDetailedHourStatisticForLastDay() {

        List<HourStatistic> result = new ArrayList<>();
        Date currentTime = new Date();
        List<Sale> sales = saleRepository.findAll();
        Date startHour = new Date((DateUtils.truncate(currentTime,10)).getTime()-90000000l);

        for (long time = startHour.getTime(); time < currentTime.getTime()-3600000l; time += 3600000l) {
            HourStatistic hourStatistic = new HourStatistic();
            hourStatistic.setDate(new Date(time));
            result.add(hourStatistic);
        }

        for (HourStatistic hourStatistic : result) {
        for (Sale sale : sales) {
                if (hourStatistic.getDate().getTime() == DateUtils.truncate(sale.getDate(),10).getTime()) {
                    hourStatistic.setBills(hourStatistic.getBills()+1);
                    hourStatistic.setBillsSum(hourStatistic.getBillsSum() + (double)(((sale.getAmount()*sale.getProducts().iterator().next().getPrice())/100)*(100-sale.getDiscount())));
                    hourStatistic.setDiscountSum(hourStatistic.getDiscountSum() + (double)(((sale.getAmount()*sale.getProducts().iterator().next().getPrice())/100)*sale.getDiscount()));
                }
            }
        }

        result.sort(new Comparator<HourStatistic>() {
            @Override
            public int compare(HourStatistic o1, HourStatistic o2) {
                return o1.getDate().before(o2.getDate()) ? 1 : -1;
            }
        });

      return result;
    }

}
