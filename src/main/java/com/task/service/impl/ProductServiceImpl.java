package com.task.service.impl;

import com.task.entity.Product;
import com.task.repository.ProductRepository;
import com.task.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Random;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;


    @Override
    public List<Product> getAllProducts() {
        List<Product> Product = productRepository.findAll();
        Product.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return (int) (o1.getId() - o2.getId());
            }
        });
        return Product;
    }

    @Override
    public Product addProduct(Product Product) {
        return productRepository.saveAndFlush(Product);
    }

    @Override
    public Product getProduct(Long id) {
        return productRepository.findOne(id);
    }

    @Override
    public Product updateProduct(Product Product) {
        return productRepository.saveAndFlush(Product);
    }

    @Override
    public void removeProduct(Long id) {
        productRepository.delete(id);
    }

    @Override
    public Product getLuckyProduct() {
        return new Product();
    }
}
